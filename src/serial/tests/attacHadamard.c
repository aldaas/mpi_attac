/** \file attacHadamard.c
 * \author Hussam Al Daas
 * \brief TT tensors operations
 * \date 16/07/2019
 * \details
*/
#include <string.h>
#include <math.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	int d = 3;
	int *n = NULL;
	int *r = NULL;

	n = (int*) malloc(d * sizeof(int));
	r = (int*) malloc((d + 1) * sizeof(int));

	double **x = NULL;

	x = (double**) malloc(d * sizeof(double*));
	r[0] = 1;
	r[d] = 1;
	n[0] = 2;
	for(int i = 1; i < d; ++i){
		r[i] = 2;
		n[i] = i + 2;
	}

	int nmax = 0;
	int rmax = 0;
	for(int i = 0; i < d; ++i){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}

	for(int i = 0; i < d; ++i){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
	}

	srand(1);
	
	for(int i = 0; i < d; ++i){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = (double) j;
	}

	int *worki = NULL;
	worki = (int*) malloc( (d + 1) * sizeof(int));

	double *workd = NULL;

	workd = (double*) malloc((rmax * rmax + nmax +  (2 * rmax) * (2 * rmax) * (2 * rmax) * (2 * rmax) * nmax) * sizeof(double));

	/**
	 * Hadamard product of TT tensors
	 */

	double **xHx = NULL;
	xHx = (double**) malloc(d * sizeof(double*));

	int *rxHx = NULL;
	rxHx = (int*) malloc((d + 1) * sizeof(int));

	for(int i = 0; i < d + 1; ++i){
		rxHx[i] = r[i] * r[i];
	}
	for(int i = 0; i < d; ++i){
		xHx[i] = (double*) malloc(n[i] * rxHx[i] * rxHx[i + 1] * sizeof(double));
	}

	attacHadamard(d, n, r, x, r, x, rxHx, xHx);

  double ExactxHx0[2*4] = {0, 1, 0, 3, 0, 3, 4, 9};
  double ExactxHx1[4*3*4] = {0, 0, 0, 1, 4, 6, 6, 9, 16, 20, 20, 25, 0, 0, 6, 7, 16, 18, 24, 27, 40, 44, 50, 55, 0, 6, 0, 7, 16, 24, 18, 27, 40, 50, 44, 55, 36, 42, 42, 49, 64, 72, 72, 81, 100, 110, 110, 121};
  double ExactxHx2[4*4] = {0, 0, 0, 1, 4, 6, 6, 9,16, 20, 20, 25, 36, 42, 42, 49};

  double **ExactxHx = NULL;
  ExactxHx = (double**) malloc(d * sizeof(double*));
  ExactxHx[0] = ExactxHx0;
  ExactxHx[1] = ExactxHx1;
  ExactxHx[2] = ExactxHx2;
  for(int i = 0; i < d; i++){
    for(int j = 0; j < n[i] * rxHx[i] * rxHx[i + 1]; j++){
      xHx[i][j] -= ExactxHx[i][j];
      if(fabs(xHx[i][j]) > 1e-15){
        fprintf(stderr, "Incorrect Hadamard product\n");
        return 1;
      }
    }
  }
  free(ExactxHx);
	free(rxHx);
	for(int i = 0; i < d; ++i){
		free(x[i]);
		free(xHx[i]);
	}
	free(x);
	free(xHx);
	free(workd);
	free(worki);
	free(n);
	free(r);
  return 0;
}


