/** \file attac_Comparison.c
 * \author Hussam Al Daas
 * \brief Parallel TT tensors operations
 * \date 14/06/2020
 * \details
*/
#include <string.h>
#include <math.h>
#include <time.h>
#include "utilities.h"
#include "attac.h"

int main(int argc, char* argv[]){
	int d = 30;
	int *n = NULL;
	int *r = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		exit(1);
	}
	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		exit(1);
	}

	double **x = NULL;
	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		exit(1);
	}
	r[0] = 1;
	r[d] = 1;
	n[0] = 1000;
	for(int i = 1; i < d; ++i){
		r[i] =  5 + abs(d/4 - abs(d/2 - i)/2);
		n[i] = n[0] + 10 * i;// + 100 * i;
	}
  r[d/2] = 50;

  printf("**************************\n");
  printf("* Tensor Order: %d\n", d);
  printf("* Tensor Mode Sizes: \n");
  for(int i = 0; i < d - 1; i++){
    printf("n[%d] = %d, ", i, n[i]);
  }
	printf("n[%d] = %d\n", d - 1, n[d - 1]);
  printf("**************************\n");

	int nmax = 0;
	int rmax = 0;
	int Srr = 0;
	for(int i = 0; i < d; i++){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}
	for(int i = 1; i < d; i++){
		Srr += 4 * r[i] * r[i];
	}
	Srr += 4 * rmax * rmax;

	for(int i = 0; i < d; i++){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
		  exit(1);
		}
	}

	srand(1);
	
	for(int i = 0; i < d; i++){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
	}
	for(int i = 0; i < d; i++){
		double normxi = 0;
    int rn = r[i] * n[i];
	  normxi = dlange_("F", &rn, &r[i + 1], x[i], &rn, NULL);
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = x[i][j] / normxi;
	}

	double *workd = NULL;

  // max (n[i] rxpx[i] rxpx[i + 1])
  int maxnrr = (r[1] * n[0] > r[d - 1] * n[d - 1]) ? 2 * r[1] * n[0] : 2 * r[d - 1] * n[d - 1];

  for(int i = 1; i < d - 1; i++){
    maxnrr = (4 * n[i] * r[i] * r[i + 1] > maxnrr) ? 4 * n[i] * r[i] * r[i + 1] : maxnrr;
  }

	int lworkd =   Srr                      // lv
               + (2 * rmax) * (2 * rmax)  // work
               + (2 * rmax) * (2 * rmax)  // R
               + (2 * rmax) * (2 * rmax)  // tau
               + 2 * (2 * rmax)           // Singular values
               + maxnrr;                  // work for Orthogonalization/compression/NormHadamardOpt
	workd = (double*) malloc(lworkd * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		exit(1);
	}

	int *indices = NULL;
	indices = (int*) malloc(d * sizeof(double));
	if(indices == NULL){
		fprintf(stderr, "indices was not well allocated\n");
		exit(1);
	}
	for( int i = 0; i < d; i++){
		indices[i] = i % n[i];
	}
	double val = 0; 
	double val_L, val_R, val_LI, val_RI;
	/**
	 * Runtime: Left to Right compression 
	 */

	/**< Normalizing the tensor */
	double norm = attacNormSquaredHadamardOpt(d, n, r, x, workd);
	attacScale(1./sqrt(norm), 0, d, n, r, x);

	double **xpx = NULL;
	xpx = (double**) malloc(d * sizeof(double*));
	if(xpx == NULL){
		fprintf(stderr, "xpx was not well allocated\n");
		exit(1);
	}

	int *rxpx = NULL;
	rxpx = (int*) malloc((d + 1) * sizeof(int));
	if(rxpx == NULL){
		fprintf(stderr, "rxpx was not well allocated\n");
		exit(1);
	}
	rxpx[0] = 1;
	rxpx[d] = 1;
	for(int i = 1; i < d; i++){
		rxpx[i] = 2 * r[i];
	}
	for(int i = 0; i < d; i++){
		xpx[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(xpx[i] == NULL){
			fprintf(stderr, "xpx[%d] was not well allocated\n", i);
			exit(1);
		}
	}

	double alpha = 2.0;
	double beta = -1.0;

	double timing_L, timing_R, timing_LI, timing_RI;
	double threshold = 1e-5/sqrt((double)(d - 1));

  clock_t begin = 0, end = 0;

	val = attacValue(indices, d, n, r, x, workd);
  printf("Val = %f\n", val);
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);

  begin = clock();
  attacCompressImplicitQs(d, n, rxpx, xpx, threshold, 'R', workd);
  end = clock();
	timing_RI = (double)(end - begin) / CLOCKS_PER_SEC;
	
  printf("Ranks of LRLI compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);
	val_RI = attacValue(indices, d, n, rxpx, xpx, workd);
	/*****************/
  //////////////////////////////////////////////////////////////////////
	/* Left to Right Compression */
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);
	
  begin = clock();
  attacCompressImplicitQs(d, n, rxpx, xpx, threshold, 'L', workd);
  end = clock();
	timing_LI = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Ranks of RLRI compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);
	val_LI = attacValue(indices, d, n, rxpx, xpx, workd);
	/*****************/
  //////////////////////////////////////////////////////////////////////
	/* Left to right Compression */
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);

  begin = clock();
  attacCompress(d, n, rxpx, xpx, threshold, 'L', workd);
  end = clock();
	timing_L = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Ranks of RLR compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; ++i){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);
	val_L = attacValue(indices, d, n, rxpx, xpx, workd);
	/*****************/
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	attacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);

  begin = clock();
  attacCompress(d, n, rxpx, xpx, threshold, 'R', workd);
  end = clock();
	timing_R = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Ranks of LRL compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		printf("%d, ", rxpx[i]);
	}
	printf("%d\n", rxpx[d]);
	val_R = attacValue(indices, d, n, rxpx, xpx, workd);
	/*****************/


	printf("/*************************/\n");
	printf("*     Time comparison     *\n");
	printf("/*************************/\n");
	printf("pattacCompress LRL:     %f\n", timing_R);
	printf("pattacCompress RLR:     %f\n", timing_L);
	printf("pattacCompress LRLI:    %f\n", timing_RI);
	printf("pattacCompress RLRI:    %f\n", timing_LI);
	printf("/*************************/\n");
	printf("/*************************/\n");
	printf("/*************************/\n");
	printf("*     Time comparison     *\n");
	printf("/*************************/\n");
	printf("val_LRL:     %e\n", val_R);
	printf("val_RLR:     %e\n", val_L);
	printf("val_LRLI:    %e\n", val_RI);
	printf("val_RLRI:    %e\n", val_LI);
	printf("/*************************/\n");
	printf("/*************************/\n");
  if(fabs(val_R - val_L) > 1e-10 * val_R || fabs(val_R - val_RI) > 1e-10 * val_R || fabs(val_R - val_LI) > 1e-10 * val_R){
    fprintf(stderr, "Values are not equal\n");
    return 1;
  }
	for(int i = 0; i < d; i++){
		free(x[i]);
		free(xpx[i]);
	}
	free(xpx);
	free(rxpx);
	free(x);
	free(workd);
	free(n);
	free(r);
  return 0;
}

