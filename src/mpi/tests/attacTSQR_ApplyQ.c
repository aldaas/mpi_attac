/** \file attacTSQR_ApplyQ.c
 * \author Hussam Al Daas
 * \brief TSQR test
 * \date 03/10/2019
 * \details
*/
#include <string.h>
#include <mkl.h>
#include <math.h>
#include "timer.h"
#include "utilities.h"
#include "putilities.h"

int main(int argc, char* argv[]){
	MPI_Init(&argc, &argv);
	attacInitTimer();
	ATTAC_START_TIMER

	int ierr = 0, size, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Status status;

	char layout = 'R';
	int LapackLayout = (layout == 'C') ? LAPACK_COL_MAJOR : LAPACK_ROW_MAJOR;

	int treeDepth = 0;
	getTreeDepth(size, &treeDepth);

	int m = 10, n = 4, l = 3;
	int nb = n;
	int ldloca = (layout == 'C') ? m : n;
	int ldlocc = (layout == 'C') ? m : l;

	double *a = NULL, *loca = NULL, *c = NULL, *copy_loca = NULL;

	loca = (double*) malloc(m * n * sizeof(double));
	copy_loca = (double*) malloc(m * n * sizeof(double));
	c = (double*) malloc(m * n * sizeof(double));
	a = (double*) malloc(size * m * n * sizeof(double));
	if(a == NULL || copy_loca == NULL || c == NULL || a == NULL){
		fprintf(stderr, "Memory allocation did not take place as expected\n");
		MPI_Abort(MPI_COMM_WORLD, -999);
	}
	
	// Send local matrices
	srand(1);
	for(int i = 0; i < size * m; ++i){
		for(int j = 0; j < n; ++j){
			if(layout == 'C'){
				a[i + j * size * m] = ((double)rand())/RAND_MAX;
			}else{
				a[i * n + j] = ((double)rand())/RAND_MAX;
			}
		}
	}
	// Send Global A
	if(!rank){
		for(int i = 1; i < size; ++i){
			ierr = MPI_Send(a, m * size * n, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
		}
	}else{
		ierr = MPI_Recv(a, m * n * size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
	}

	double* tempa = (layout == 'C') ? a + m * rank : a + m * n * rank;
	int lda = (layout == 'C') ? m * size : n;
	ierr = LAPACKE_dlacpy(LapackLayout, 'A', m, n, tempa, lda, loca, ldloca);
	ierr = LAPACKE_dlacpy(LapackLayout, 'A', m, n, tempa, lda, copy_loca, ldloca);

	// Compute QR of distributed A (TSQR)
	double *work = NULL;

	int lv = (treeDepth + 1) * n * n; // v
	int lt = (treeDepth + 1) * n * nb; // t
	int lr = n * n;               // R
	int lrtsqr = n * n;           // RTSQR
	int offset = 0;

	int lwork = n * n  // work
						+ lv     // v
					  + lt     // t
						+ lr     // R
						+ lrtsqr // RTSQR
						;

	work = (double*) malloc(lwork * sizeof(double));
	if(work == NULL){
		fprintf(stderr, "Memory allocation for work place did not take place as expected\n");
		MPI_Abort(MPI_COMM_WORLD, -999);
	}
	offset += n * n;

	double* R;
	R = work + offset;
	offset += lr;

	double* RTSQR = work + offset;
	offset += lrtsqr;

	double *v = work + offset;
	offset += lv;

	double *t = work + offset;
	offset += lt;

	memset(v, 0, lv * sizeof(double));
	memset(t, 0, lt * sizeof(double));

	attacTSQR(MPI_COMM_WORLD, layout, m, n, nb, loca, ldloca, RTSQR, v, t, work);
	
	if(!rank){
		setLowerZero(layout, n, n, RTSQR, n);
	}
	ierr = MPI_Bcast(RTSQR, n * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	// Compute Q factor TSQR
	memset(c, 0, m * n * sizeof(double));
	
	MPI_Barrier(MPI_COMM_WORLD);

	for(int i = 0; i < l; ++i){
		c[i + i * ldlocc] = 1.;
	}

	if(rank) l = m;
  attacTSQRApplyQ(MPI_COMM_WORLD, layout, m, &l, n, nb, loca, ldloca, v, t, c, ldlocc, work);
	for(int i = 0; i < size; i++){
		if(rank == i){
			printf("proc %d: l = %d\n", rank, l);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}

	////////////////////////

	// Compute Q \times R - A

	CBLAS_LAYOUT cblaslayout = (layout == 'C') ? CblasColMajor : CblasRowMajor;
	cblas_dgemm (cblaslayout, CblasNoTrans, CblasNoTrans, m, l, l, 1., c, ldlocc, RTSQR, n, -1., copy_loca, ldloca);
	for(int i = 0; i < m; ++i){
		for(int j = l; j < n; ++j){
			if(layout == 'C') copy_loca[i + ldloca * j] = 0;
			if(layout == 'R') copy_loca[j + i * ldloca] = 0;
		}
	}

	double maxa = -1.;
	double maxdif = -1.;
	double dif = 0;
	for(int j = 0; j < n * m * size; ++j){
		maxa = (maxa > fabs(a[j])) ? maxa : fabs(a[j]);
	}

	for(int j = 0; j < n; ++j){
		for(int i = 0; i < m; ++i){
			dif = (layout == 'C') ? fabs(copy_loca[i + j * m]) : fabs(copy_loca[i * n + j]);
			maxdif = (maxdif > dif) ? maxdif : dif;
		}
	}
	MPI_Allreduce(MPI_IN_PLACE, &maxdif, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if(!rank){
		printf("Max(|Q * R - A|) = %e\n", maxdif);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	for(int i = 0; i < size; ++i){
		if(maxdif > 1e-12 * maxa && rank == i){
			printf("\n**************************************************\n");
			printf("\t Matrix rank %d\n", i);
			printf("max_{i,j} || A || = %.14f\n", maxa);
			printf("max_{i,j} || Q R - A || = %.14f\n", maxdif);
			printf("\n**************************************************\n");
			printf("a\n");
			int lda = (layout == 'C') ? m * size : n;
			printMatrix(layout, m * size, n, a, lda);
			printf("Q\n");
			printMatrix(layout, m, n, c, ldloca);
			printf("R\n");
			printMatrix(layout, n, n, RTSQR, n);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}

	if(!rank){
		printf("\n**************************************************\n");
		printf("\n**************************************************\n");
		printf("R \n");
		printf("\n**************************************************\n");
		printf("\n**************************************************\n");
		printMatrix(layout, n, n, RTSQR, n);
		printf("\n**************************************************\n");
		printf("\n**************************************************\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);

	// Compute Q^T Q - I
	if(!rank){
		printf("\n**************************************************\n");
		printf("\n**************************************************\n");
	}
	cblas_dgemm (cblaslayout, CblasTrans, CblasNoTrans, l, l, m, 1., c, ldlocc, c, ldlocc, 0.0, copy_loca, l);
	MPI_Allreduce(MPI_IN_PLACE, copy_loca, l * l, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	for(int k = 0; k < l; ++k){
		copy_loca[k + l * k] -= 1.;
	}
	double maxQtQMinusI = 0;
	for(int k = 0; k < l * l; ++k){
		maxQtQMinusI = (fabs(copy_loca[k]) > maxQtQMinusI) ? fabs(copy_loca[k]) : maxQtQMinusI;
	}
	MPI_Allreduce(MPI_IN_PLACE, &maxQtQMinusI, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	if(!rank){
		printf("Max(| Q^T Q - I | = %e\n", maxQtQMinusI);
	}
	if(!rank){
		printf("\n**************************************************\n");
		printf("\n**************************************************\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);
	if(!rank){
		printf("free data\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);

	free(c);
	free(a);
	free(loca);
	free(copy_loca);
	free(work);

	MPI_Barrier(MPI_COMM_WORLD);
	if(!rank){
		printf("STOP Timer\n");
	}
  ATTAC_STOP_TIMER
	MPI_Barrier(MPI_COMM_WORLD);
	if(!rank){
		printf("print timer\n");
	}
	MPI_Barrier(MPI_COMM_WORLD);
	attacPrintTimer("Timing.txt");
	attacFinalizeTimer();
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Finalize();
  return 0;
}

