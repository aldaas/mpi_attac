/** \file pattacBFROrtho_BFRCompress_Implicit.c
 * \author Hussam Al Daas
 * \brief Parallel TT tensors operations
 * \date 14/06/2020
 * \details
*/
#include <string.h>
#include <math.h>
#include <mkl.h>
#include <unistd.h>
#include <stdbool.h>
#include <getopt.h>
#include <stdlib.h>
#include "timer.h"
#include "utilities.h"
#include "pattac.h"

int main(int argc, char* argv[]){
	MPI_Init(&argc, &argv);

	int opt;
	int d = 30; // number of dimensions
	int I = 1000; // size in each dimension
	int R = 5; // 1, R, ..., R, 1

	bool rel = false;

	while ((opt = getopt(argc, argv, "d:I:R:r")) != -1) {
		switch (opt) {
			case 'd':
				d = atoi(optarg);
				break;
			case 'I':
				I = atoi(optarg);
				break;
			case 'R':
				R = atoi(optarg);
				break;
			case 'r':
				rel = true;
				break;
		}
	}

	/** Timer set up*/
	char *filename;
  filename = (char*) malloc(100 * sizeof(char));
	attacInitTimer();
	ATTAC_START_TIMER
	int size = 0, rank = 0;
	MPI_Comm comm = MPI_COMM_WORLD;
	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

  if(!rank){
    printf("**************************\n");
    printf("* Number of processors: %d\n", size);
    printf("**************************\n");
  }

	/**< Binary Tree Depth */

	int *n = NULL;
	int *r = NULL;
	int *isRedundant = NULL;

	n = (int*) malloc(d * sizeof(int));
	if(n == NULL){
		fprintf(stderr, "n was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r = (int*) malloc((d + 1) * sizeof(int));
	if(r == NULL){
		fprintf(stderr, "r was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	isRedundant = (int*) malloc(d * sizeof(int));
	if(isRedundant == NULL){
		fprintf(stderr, "isRedundant was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	double **x = NULL;

	x = (double**) malloc(d * sizeof(double*));
	if(x == NULL){
		fprintf(stderr, "x was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	r[0] = 1;
	r[d] = 1;
	n[0] = I;
	isRedundant[0] = 0;
	for(int i = 1; i < d; ++i){
		r[i] =  R + abs(d/4 - abs(d/2 - i)/2);
		n[i] = I;
    isRedundant[i] = 0;
	}
  r[d/2] = 50;
  //r[1] = 10;
	isRedundant[d-1] = 0;

	if(!rank){
    printf("**************************\n");
    printf("* Tensor Order: %d\n", d);
    printf("* Tensor Mode Sizes: \n");
    for(int i = 0; i < d - 1; i++){
      printf("n[%d] = %d, ", i, n[i]);
    }
		printf("n[%d] = %d\n", d - 1, n[d - 1]);
    printf("* Modes Redundancy:  \n");
    for(int i = 0; i < d - 1; i++){
      printf("%d, ", isRedundant[i]);
    }
    printf("%d\n", isRedundant[d - 1]);
    printf("**************************\n");
  }
  MPI_Barrier(comm);

	int nmax = 0;
	int rmax = 0;
	int Srr = 0;
	for(int i = 0; i < d; i++){
		nmax = (n[i] > nmax) ? n[i] : nmax;
		rmax = (r[i] > rmax) ? r[i] : rmax;
	}
	for(int i = 1; i < d; i++){
		Srr += 4 * r[i] * r[i];
	}
	Srr += 4 * rmax * rmax;

	for(int i = 0; i < d; i++){
		x[i] = (double*) malloc(n[i] * r[i] * r[i + 1] * sizeof(double));
		if(x[i] == NULL){
			fprintf(stderr, "x[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	srand(rank);
	
	for(int i = 0; i < d; i++){
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = ((double)rand())/RAND_MAX;
    MPI_Barrier(MPI_COMM_WORLD);
    if(isRedundant[i] == 1){
      int ierr = 0;
      ierr = MPI_Bcast(x[i], r[i] * n[i] * r[i + 1], MPI_DOUBLE, 0, MPI_COMM_WORLD);
		  if(ierr != 0){
		  	fprintf(stderr, "%s:Line %d %s::MPI_Allreduce:: ierr = %d\n", __FILE__, __LINE__, __func__, ierr);
		  	MPI_Abort(MPI_COMM_WORLD, ierr);
		  }
    }
	}
	for(int i = 0; i < d; i++){
		double normxi = 0;
	  normxi = LAPACKE_dlange(LAPACK_COL_MAJOR, 'F', r[i] * n[i], r[i + 1], x[i], r[i] * n[i]);
    if(isRedundant[i] == 0){
	  	normxi *= normxi;
	    MPI_Allreduce(MPI_IN_PLACE, &normxi, 1, MPI_DOUBLE, MPI_SUM, comm);
	  	normxi = sqrt(normxi);
    }
		for(int j = 0; j < r[i] * n[i] * r[i + 1]; ++j)
		  x[i][j] = x[i][j] / normxi;
	}

	double *workd = NULL;

  // max (n[i] rxpx[i] rxpx[i + 1])
  int maxnrr = (r[1] * n[0] > r[d - 1] * n[d - 1]) ? 2 * r[1] * n[0] : 2 * r[d - 1] * n[d - 1];

  for(int i = 1; i < d - 1; i++){
    maxnrr = (4 * n[i] * r[i] * r[i + 1] > maxnrr) ? 4 * n[i] * r[i] * r[i + 1] : maxnrr;
  }

  int CLog2P = (int) ceil(log2((double)size));
	int lworkd =   Srr * (CLog2P + 1)                     // lv
               + Srr * (CLog2P + 1)                     // lt
               + (2 * (2 * rmax) + 1) * (2 * rmax)      // work
               + (2 * rmax) * (2 * rmax)                // R
               + 2 * (2 * rmax)                         // Singular values
               + maxnrr;                                // work for Orthogonalization/compression/NormHadamardOpt
	workd = (double*) malloc(lworkd * sizeof(double));
	if(workd == NULL){
		fprintf(stderr, "workd was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	int *indices = NULL, *rankIndices = NULL;
	indices = (int*) malloc(d * sizeof(double));
	if(indices == NULL){
		fprintf(stderr, "indices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	rankIndices = (int*) malloc(d * sizeof(double));
	if(rankIndices == NULL){
		fprintf(stderr, "rankIndices was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	for( int i = 0; i < d; i++){
		indices[i] = i % n[i];
		rankIndices[i] = i % size;
	}
	double val = 0; 
	/**
	 * Runtime: Left to Right compression 
	 */

	/**< Normalizing the tensor */
	double norm = pattacNormSquaredHadamardOpt(comm, isRedundant, d, n, r, x, workd);
	pattacScale(1./sqrt(norm), 0, d, n, r, x);

	double **xpx = NULL;
	xpx = (double**) malloc(d * sizeof(double*));
	if(xpx == NULL){
		fprintf(stderr, "xpx was not well allocated\n");
		MPI_Abort(comm, 100);
	}

	int *rxpx = NULL;
	rxpx = (int*) malloc((d + 1) * sizeof(int));
	if(rxpx == NULL){
		fprintf(stderr, "rxpx was not well allocated\n");
		MPI_Abort(comm, 100);
	}
	rxpx[0] = 1;
	rxpx[d] = 1;
	for(int i = 1; i < d; i++){
		rxpx[i] = 2 * r[i];
	}
	for(int i = 0; i < d; i++){
		xpx[i] = (double*) malloc(n[i] * rxpx[i] * rxpx[i + 1] * sizeof(double));
		if(xpx[i] == NULL){
			fprintf(stderr, "xpx[%d] was not well allocated\n", i);
			MPI_Abort(comm, 100);
		}
	}

	double alpha = 2.0;
	double beta = -1.0;

	double timing1;
	double threshold = 1e-5/sqrt((double)(d - 1));

	val = pattacValue(comm, indices, rankIndices, d, n, r, x, workd);
	attacResetTimer();
	MPI_Barrier(comm);
  //////////////////////////////////////////////////////////////////////
	/* Right to left Compression */
	/**< Formal AXPBY 1 */
	pattacFormalAXPBY(d, n, alpha, r, x, beta, r, x, rxpx, xpx);

	if(!rank) printf("Ranks of formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
	val = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
	timing1 = MPI_Wtime();
  pattacBFCompressImplicitQs(comm, isRedundant, d, n, rxpx, xpx, threshold, 'L', workd);
	timing1 = (MPI_Wtime() - timing1);
	if(!rank) printf("Ranks of compressed formal %f x + (%f) x \n", alpha, beta);
	for(int i = 0; i < d; i++){
		if(!rank) printf("%d, ", rxpx[i]);
	}
	if(!rank) printf("%d\n", rxpx[d]);
  if(!rank){
    printf("value before pattacBFCompressImplicitQs: %e\n", val);
  }
	val = pattacValue(comm, indices, rankIndices, d, n, rxpx, xpx, workd);
  if(!rank){
    printf("value after pattacBFCompressImplicitQs: %e\n", val);
  }
	MPI_Barrier(comm);
	/** Print timing */
  sprintf(filename, "%s_%.5d.txt", "Timing_pattacBFCompressImplicit_L", size);
	printTSQRTimer(filename, comm);
  if(!rank) attacPrintTimer(filename);
	attacResetTimer();
	/*****************/
  MPI_Barrier(comm);

	for(int i = 0; i < d; i++){
		free(x[i]);
		free(xpx[i]);
	}
	free(xpx);
	free(rxpx);
	free(x);
	free(workd);
	free(n);
	free(r);
	free(isRedundant);
  ATTAC_STOP_TIMER
	free(filename);
	attacFinalizeTimer();
	MPI_Finalize();
  return 0;
}

