/** \copyright
 * Copyright 2021 Hussam Al Daas, Grey Ballard, and Peter Benner
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/** \file timer.h
 * \author Hussam Al Daas
 * \brief timer
 * \date 01/10/2019
 * \details 
*/
#ifndef TIMER_H
#define TIMER_H

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#define MAXNTIC 15
#define TIME_BREAKDOWN 0

#define ATTAC_START_TIMER static attacFuncTimer ATTAC_t = {.funcName = __func__, .ncall = {0}, .subTimer = {0}, .subTimerName = {NULL}, .isDefined = 0};\
                            double ATTAC_tic[MAXNTIC]  = {0};\
														double ATTAC_tac;\
																	 ATTAC_tac = 0;\
														double TSQR_COMM_TIME;\
														TSQR_COMM_TIME = 0;\
														double TSQR_COMP_TIME;\
														TSQR_COMP_TIME = 0;\
														double TSQR_IDLE_TIME;\
														TSQR_IDLE_TIME = 0;\
														double ATTAC_TSQR_tac;\
														ATTAC_TSQR_tac = 0;\
                            if(!ATTAC_t.isDefined){ \
															attacAddTimer(&ATTAC_t); ATTAC_t.isDefined = 1;}\
                            ATTAC_tic[0] = MPI_Wtime();

#define ATTAC_TIC(_id, _name) ATTAC_tic[(_id)] = MPI_Wtime();\
                            ATTAC_t.subTimerName[(_id)] = (_name);

#define ATTAC_TAC(_id)   ATTAC_tac = MPI_Wtime();\
                                ATTAC_tic[(_id)] = ATTAC_tac - ATTAC_tic[(_id)];\
                                ATTAC_t.subTimer[(_id)] += ATTAC_tic[(_id)];\
                                ATTAC_t.ncall[(_id)]++;

#define ATTAC_STOP_TIMER  ATTAC_tic[0] = MPI_Wtime() - ATTAC_tic[0];\
                              ATTAC_t.subTimer[0] += ATTAC_tic[0];\
                              ATTAC_t.ncall[0]++;\
														if(strcmp(ATTAC_t.funcName, "attacTSQR") * strcmp(ATTAC_t.funcName, "attacBFTSQR") == 0){\
															TSQRTimerAdd(0, TSQR_COMP_TIME, 0, TSQR_IDLE_TIME, 1);\
														}\
														if(strcmp(ATTAC_t.funcName, "attacTSQRApplyQ") * strcmp(ATTAC_t.funcName, "attacBFTSQRApplyQ") == 0){\
															ApplyTSQRTimerAdd(0, TSQR_COMP_TIME, 0, TSQR_IDLE_TIME, 1);\
														}\
														if(strcmp(ATTAC_t.funcName, "attacTSQRUp") * strcmp(ATTAC_t.funcName, "attacBFTSQRUp") == 0){\
															TSQRTimerAdd(TSQR_COMM_TIME, 0, TSQR_COMP_TIME, TSQR_IDLE_TIME, 0);\
														};\
														if(strcmp(ATTAC_t.funcName, "attacTSQRUpApplyQ") * strcmp(ATTAC_t.funcName, "attacBFTSQRUpApplyQ") == 0){\
															ApplyTSQRTimerAdd(TSQR_COMM_TIME, 0, TSQR_COMP_TIME, TSQR_IDLE_TIME, 0);\
														};

#define COMP_TIMER_TIC ATTAC_TSQR_tac = MPI_Wtime();

#define COMP_TIMER_TAC ATTAC_TSQR_tac = MPI_Wtime() - ATTAC_TSQR_tac;\
																	addCOMPTime(ATTAC_TSQR_tac);

#define BCAST_TIMER_TIC ATTAC_TSQR_tac = MPI_Wtime();

#define BCAST_TIMER_TAC ATTAC_TSQR_tac = MPI_Wtime() - ATTAC_TSQR_tac;\
																	addBCASTTime(ATTAC_TSQR_tac);

#define TSQR_COMM_TIC ATTAC_TSQR_tac = MPI_Wtime();

#define TSQR_COMM_TAC TSQR_COMM_TIME += MPI_Wtime() - ATTAC_TSQR_tac;
											
#define TSQR_COMP_TIC ATTAC_TSQR_tac = MPI_Wtime();

#define TSQR_COMP_TAC TSQR_COMP_TIME += MPI_Wtime() - ATTAC_TSQR_tac;

#define TSQR_IDLE_TIC ATTAC_TSQR_tac = MPI_Wtime();

#define TSQR_IDLE_TAC TSQR_IDLE_TIME += MPI_Wtime() - ATTAC_TSQR_tac;

typedef struct{
	const char *funcName;
	int ncall[MAXNTIC];
	double subTimer[MAXNTIC];
	char *subTimerName[100];
	int isDefined;
} attacFuncTimer;

typedef struct{
	attacFuncTimer **t;
	int n;
	int maxn;
} attacTimer;

typedef struct{
	double *comm;
	double *compLeaf;
	double *compTree;
	double *idle;
	int idx;
	int size;
} tsqrTimer;

typedef struct{
	double bcast;
} bcastTimer;

typedef struct{
	double comp;
} compTimer;

void attacInitTimer();
void attacResetTimer();
void attacAddTimer(attacFuncTimer* t);
void attacPrintTime(attacFuncTimer *t, FILE* fd);
void attacPrintTimer(const char* filename);
void attacFinalizeTimer();
void TSQRTimerAdd(double comm, double compLeaf, double compTree, double idle, int flag);
void ApplyTSQRTimerAdd(double comm, double compLeaf, double compTree, double idle, int flag);
void addBCASTTime(double val);
void addCOMPTime(double val);
void printTSQRTimer(const char* filename, MPI_Comm comm);
#endif
